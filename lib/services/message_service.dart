import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:messenger_app/models/chat.dart';
import 'package:messenger_app/models/message.dart';

class MessageService {
  MessageService._();

  static final _instance = MessageService._();

  factory MessageService.getInstance() => _instance;

  Future<dynamic> onSendMessage({Chat chat, Message message}) async {
    Firestore.instance
        .collection('chat-groups')
        .document(chat.chatId.toString())
        .collection('chat-messages')
        .document(message.messageId.toString())
        .setData(
          Message.toJson(message),
        );
  }

  Future<Map<String, Message>> fetchChatMessages({Chat chat}) async {
    final Map<String, Message> messageMap = await Firestore.instance
        .collection('chat-groups')
        .document(chat.chatId.toString())
        .collection('chat-messages')
        .orderBy('timeStamp', descending: true)
        .getDocuments()
        .then((snapshot) {
      Map<String, Message> map = Map<String, Message>();
      snapshot.documents.forEach((value) {
        Message message = Message.fromJson(value.data);
        map.addAll(<String, Message>{message.messageId.toString(): message});
      });
      return map;
    });
    return messageMap;
  }
}
