import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messenger_app/models/user.dart';

class FirebaseUserService {
  FirebaseUserService._();

  factory FirebaseUserService.getInstance() => _instance;
  static final _instance = FirebaseUserService._();

  FirebaseUser firebaseUser;

  Future<FirebaseUser> signUp({
    String email,
    String password,
    BuildContext context,
    User user,
  }) async {
    await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((AuthResult authResult) async {
      firebaseUser = await authResult.user;
      await signUpComplete(firebaseUser, context, user, firebaseUser.uid);
      return (await firebaseUser);
    }).catchError((e) {
      print('sign up error $e');
    });
    return (await firebaseUser);
  }

  void signUpComplete(FirebaseUser firebaseUser, BuildContext context,
      User user, String documnetId) {
    Firestore.instance.collection('/users').document(documnetId).setData({
      'email': firebaseUser.email,
      'uid': firebaseUser.uid,
      'id': user.id.toString(),
      'first_name': user.firstName,
      'last_name': user.lastName,
      'age': user.age.toString(),
      'gender': user.gender,
    }).catchError((e) {
      print('sign up error $e');
    });
  }

  Future<FirebaseUser> login({String email, String password}) async {
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password)
        .then((AuthResult authResult) async {
      print('logged in');
      firebaseUser = await authResult.user;
    }).catchError((e) {
      print('login error $e');
    });
    return (await firebaseUser);
  }

  Future<FirebaseUser> getCurrentUser() async {
    final FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    return currentUser;
  }

  Future<dynamic> fetchAllUsers() async {
    final FirebaseUser currentUser =
        await FirebaseAuth.instance.currentUser().then((user) {
      return user;
    });
    final Map<String, User> userMap = await Firestore.instance
        .collection('users')
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      Map<String, User> map = Map<String, User>();
      snapshot.documents.forEach((value) {
        if (value['uid'] != currentUser.uid) {
          User user = User();
          user.id = int.parse(value['id']);
          user.uid = value['uid'];
          user.age = int.parse(value['age']);
          user.gender = value['gender'];
          user.firstName = value['first_name'];
          user.lastName = value['last_name'];
          map.addAll(<String, User>{user.id.toString(): user});
        }
      });
      return map;
    });
    return userMap;
  }

  Future<User> fetchCurrentUserDetails() async {
    FirebaseUser currentUser = await getCurrentUser().then((value) {
      return value;
    });
    Map<String, dynamic> response;
    User user = User();
    final Map<String, dynamic> responseData = await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .get()
        .then((DocumentSnapshot snapshot) {
      response = snapshot.data;
      return response;
    });
    user.firstName = responseData['first_name'];
    user.lastName = responseData['last_name'];
    user.age = int.parse(responseData['age']);
    user.gender = responseData['gender'];
    user.id = int.parse(responseData['id']);
    user.uid = responseData['uid'];
    return user;
  }

  Future<dynamic> logOut() async {
    await FirebaseAuth.instance.signOut();
    return 'user logged out successfully';
  }
}
