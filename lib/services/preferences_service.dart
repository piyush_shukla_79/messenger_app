import 'dart:convert';

import 'package:messenger_app/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesService {
  PreferencesService._();

  factory PreferencesService.getInstance() => _instance;

  static final _instance = PreferencesService._();

  static const String AUTH_TOKEN = 'auth_token';
  static const String LOGGED_IN_USER = 'logged_in_user';
  static const String LOGGED_IN_USER_ID = 'logged_in_user_id';

  Future<SharedPreferences> _getInstance() async {
    return SharedPreferences.getInstance();
  }

  Future<void> setAuthToken(String token) async {
    (await _getInstance()).setString(PreferencesService.AUTH_TOKEN, token);
  }

  Future<String> getAuthToken() async {
    return (await _getInstance()).getString(PreferencesService.AUTH_TOKEN);
  }

  Future<void> setAuthUser(User user) async {
    if (user != null) {
      (await _getInstance()).setString(
          PreferencesService.LOGGED_IN_USER, json.encode(User.toJson(user)));
    } else {
      (await _getInstance())
          .setString(PreferencesService.LOGGED_IN_USER, user.toString());
    }
  }

  Future<User> getAuthUser() async {
    final String user =
        (await _getInstance()).getString(PreferencesService.LOGGED_IN_USER);
    if (user == null) {
      return null;
    }
    return User.fromJson(json.decode(user));
  }

  Future<void> setAuthUserId(String id) async {
    (await _getInstance())
        .setString(PreferencesService.LOGGED_IN_USER_ID, id);
  }

  Future<String> getAuthUserId() async {
    return (await _getInstance())
        .getString(PreferencesService.LOGGED_IN_USER_ID);
  }
}
