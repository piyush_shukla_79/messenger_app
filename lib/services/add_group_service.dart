import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:messenger_app/models/chat.dart';
import 'package:messenger_app/models/group.dart';
import 'package:messenger_app/models/user.dart';
import 'package:messenger_app/utils/globals.dart';

class AddGroupService {
  AddGroupService._();

  factory AddGroupService.getInstance() => _instance;
  static final _instance = AddGroupService._();

  Future<dynamic> addChat({Chat chat}) async {
    await Firestore.instance
        .collection('chat-groups')
        .document(chat.chatId.toString())
        .setData({
      'chat_id': chat.chatId.toString(),
      'chat_owner_id': chat.group.groupOwnerId.toString(),
      'chat_name': chat.group.groupName,
      'chat_description': chat.group.groupDescription,
      'created_at': chat.createdAt,
      'chat_logo': chat.chatLogo,
      'chat_usermap': chat.userMap,
    });
  }

  Future<Map<String, Chat>> getAllChats() async {
    final User user =
        await firebaseUserService.fetchCurrentUserDetails().then((value) {
      return value;
    });
    final Map<String, Chat> chatGroupList = await Firestore.instance
        .collection('chat-groups')
        .orderBy('created_at', descending: true)
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      Map<String, Chat> chatMap = Map<String, Chat>();
      snapshot.documents.sort((a, b) {
        return a['created_at'].compareTo(b['created_at']);
      });
      snapshot.documents.forEach((value) {
        Chat chat = Chat();
        chat.group = Group();
        chat.chatId = int.parse(value['chat_id']);
        chat.group.groupOwnerId = int.parse(value['chat_owner_id']);
        chat.group.groupName = value['chat_name'];
        chat.group.groupDescription = value['chat_description'];
        chat.createdAt = value['created_at'];
        chat.chatLogo = value['chat_logo'];
        if (value['chat_usermap'].containsKey(user.id.toString())) {
          chatMap.addAll(<String, Chat>{chat.chatId.toString(): chat});
        }
      });
      return chatMap;
    });
    return chatGroupList;
  }

  Future<dynamic> addUserToChat({Chat chat, User user}) async {
    await Firestore.instance
        .collection('chat-groups')
        .document(chat.chatId.toString())
        .collection('chat-members')
        .document(user.id.toString())
        .setData({
      'member_first_name': user.firstName,
      'member_last_name': user.lastName,
      'member_id': user.id.toString(),
      'member_age': user.age.toString(),
      'member_uid': user.uid,
      'member_gender': user.gender,
    });
  }

  Future<dynamic> fetchChatMembers({Chat chat}) async {
    final Map<String, User> chatUsersMap = await Firestore.instance
        .collection('chat-groups')
        .document(chat.chatId.toString())
        .collection('chat-members')
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      Map<String, User> userMap = Map<String, User>();
      snapshot.documents.forEach((value) {
        User user = User();
        user.id = int.parse(value['member_id']);
        user.firstName = value['member_first_name'];
        user.lastName = value['member_last_name'];
        user.age = int.parse(value['member_age']);
        user.gender = value['member_gender'];
        user.uid = value['member_uid'];
        userMap.addAll(<String, User>{user.id.toString(): user});
      });
      return userMap;
    });
    return chatUsersMap;
  }
}
