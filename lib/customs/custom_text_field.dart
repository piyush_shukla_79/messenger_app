import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/styles.dart';

class CustomTextField extends StatefulWidget {
  final String attribute;
  final List<FormFieldValidator> validators;
  final String hintText;
  String entry;

  CustomTextField({
    this.attribute,
    this.validators,
    this.hintText,
    this.entry,
  });

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: SizeConfig.screenHeight * 0.075,
      width: SizeConfig.screenWidth,
      margin: EdgeInsets.symmetric(
        vertical: 10,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 10,
      ),
      decoration: BoxDecoration(
        color: Styles.textFieldColor,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Center(
        child: FormBuilderTextField(
          attribute: widget.attribute,
          validators: widget.validators,
          onChanged: (value) {
            setState(() {
              widget.entry = value;
            });
          },
          decoration: InputDecoration(
            hintText: widget.hintText,
            border: InputBorder.none,
          ),
          maxLines: 1,
        ),
      ),
    );
  }
}
