import 'package:flutter/material.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/styles.dart';

class CustomChatTile extends StatelessWidget {
  final String title;
  final String subtitle;
  final String avatarUrl;
  final String time;
  final Function onTap;
  final Function onLongPress;
  final bool isEditing;
  final Widget checkbox;

  const CustomChatTile({
    Key key,
    this.title,
    this.subtitle,
    this.avatarUrl,
    this.time,
    this.onTap,
    this.onLongPress,
    this.isEditing,
    this.checkbox,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: SizeConfig.screenHeight * 0.125,
      width: SizeConfig.screenWidth,
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListTile(
              leading: Container(
                height: SizeConfig.screenWidth * 0.12,
                width: SizeConfig.screenWidth * 0.12,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: Styles.primaryColor,
                    width: 1.5,
                  ),
                  image: DecorationImage(
                    image: AssetImage(avatarUrl),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              title: Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(subtitle),
              trailing: isEditing ? checkbox : Text(time),
              onTap: onTap,
              onLongPress: onLongPress,
            ),
          ),
          Divider()
        ],
      ),
    );
  }
}
