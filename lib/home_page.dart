import 'package:flutter/material.dart';
import 'package:messenger_app/chats/chat_screen.dart';
import 'package:messenger_app/chats/counter_page.dart';
import 'package:messenger_app/chats/user_list_page.dart';
import 'package:messenger_app/status_manager/status_option.dart';
import 'package:messenger_app/user_manager/add_group_chat.dart';
import 'package:messenger_app/utils/styles.dart';
import 'package:messenger_app/view_profile_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: 1,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Styles.primaryColor,
          title: Text('My Chat App'),
          actions: <Widget>[
            PopupMenuButton(
              itemBuilder: (BuildContext context) {
                return <PopupMenuEntry<dynamic>>[
                  PopupMenuItem(
                    value: 'create_group',
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context, 'create_group');
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AddGroupChat()),
                        );
                      },
                      child: Text('Create Group'),
                    ),
                  ),
                  PopupMenuItem(
                    value: 'view_profile',
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context, 'view_profile');
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ViewProfilePage(),
                          ),
                        );
                      },
                      child: Text('View Profile'),
                    ),
                  ),
                ];
              },
            )
          ],
          bottom: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.camera_alt),
              ),
              Tab(
                child: Text('Chats'),
              ),
              Tab(
                child: Text('Status'),
              ),
              Tab(
                child: Text('Calls'),
              )
            ],
          ),
        ),
        body: TabBarView(
          children: [
            CounterPage(),
            ChatScreen(),
            StatusOption(),
            UserListPage(
              isEditing: false,
            ),
          ],
        ),
      ),
    );
  }
}
