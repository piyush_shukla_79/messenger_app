import 'package:json_annotation/json_annotation.dart';
import 'package:messenger_app/models/user.dart';
import 'package:mobx/mobx.dart';

part 'message.g.dart';

@JsonSerializable()
class Message extends _Message with _$Message {
  static Message fromJson(Map<String, dynamic> json) => _$MessageFromJson(json);

  static Map<String, dynamic> toJson(Message message) =>
      _$MessageToJson(message);
}

abstract class _Message with Store {
  @observable
  int messageId;

  @observable
  String data;

  @observable
  String timeStamp;

  @observable
  int senderId;

  @observable
  String senderName;
}
