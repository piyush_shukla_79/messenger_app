// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message()
    ..messageId = json['messageId'] as int
    ..data = json['data'] as String
    ..timeStamp = json['timeStamp'] as String
    ..senderId = json['senderId'] as int
    ..senderName = json['senderName'] as String;
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'messageId': instance.messageId,
      'data': instance.data,
      'timeStamp': instance.timeStamp,
      'senderId': instance.senderId,
      'senderName': instance.senderName,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Message on _Message, Store {
  final _$messageIdAtom = Atom(name: '_Message.messageId');

  @override
  int get messageId {
    _$messageIdAtom.context.enforceReadPolicy(_$messageIdAtom);
    _$messageIdAtom.reportObserved();
    return super.messageId;
  }

  @override
  set messageId(int value) {
    _$messageIdAtom.context.conditionallyRunInAction(() {
      super.messageId = value;
      _$messageIdAtom.reportChanged();
    }, _$messageIdAtom, name: '${_$messageIdAtom.name}_set');
  }

  final _$dataAtom = Atom(name: '_Message.data');

  @override
  String get data {
    _$dataAtom.context.enforceReadPolicy(_$dataAtom);
    _$dataAtom.reportObserved();
    return super.data;
  }

  @override
  set data(String value) {
    _$dataAtom.context.conditionallyRunInAction(() {
      super.data = value;
      _$dataAtom.reportChanged();
    }, _$dataAtom, name: '${_$dataAtom.name}_set');
  }

  final _$timeStampAtom = Atom(name: '_Message.timeStamp');

  @override
  String get timeStamp {
    _$timeStampAtom.context.enforceReadPolicy(_$timeStampAtom);
    _$timeStampAtom.reportObserved();
    return super.timeStamp;
  }

  @override
  set timeStamp(String value) {
    _$timeStampAtom.context.conditionallyRunInAction(() {
      super.timeStamp = value;
      _$timeStampAtom.reportChanged();
    }, _$timeStampAtom, name: '${_$timeStampAtom.name}_set');
  }

  final _$senderIdAtom = Atom(name: '_Message.senderId');

  @override
  int get senderId {
    _$senderIdAtom.context.enforceReadPolicy(_$senderIdAtom);
    _$senderIdAtom.reportObserved();
    return super.senderId;
  }

  @override
  set senderId(int value) {
    _$senderIdAtom.context.conditionallyRunInAction(() {
      super.senderId = value;
      _$senderIdAtom.reportChanged();
    }, _$senderIdAtom, name: '${_$senderIdAtom.name}_set');
  }

  final _$senderNameAtom = Atom(name: '_Message.senderName');

  @override
  String get senderName {
    _$senderNameAtom.context.enforceReadPolicy(_$senderNameAtom);
    _$senderNameAtom.reportObserved();
    return super.senderName;
  }

  @override
  set senderName(String value) {
    _$senderNameAtom.context.conditionallyRunInAction(() {
      super.senderName = value;
      _$senderNameAtom.reportChanged();
    }, _$senderNameAtom, name: '${_$senderNameAtom.name}_set');
  }
}
