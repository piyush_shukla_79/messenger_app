import 'package:json_annotation/json_annotation.dart';
import 'package:messenger_app/models/chat.dart';
import 'package:mobx/mobx.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends _User with _$User {
  static User fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static Map<String, dynamic> toJson(User user) => _$UserToJson(user);
}

abstract class _User with Store {
  @observable
  int id;

  @observable
  String uid;

  @observable
  String firstName;

  @observable
  String lastName;

  @observable
  String gender;

  @observable
  int age;

  @observable
  Map<String,Chat> chatMap;

  @computed
  String get fullName {
    if (lastName == null) {
      return '$firstName';
    } else {
      return '$firstName $lastName';
    }
  }
}
