import 'package:json_annotation/json_annotation.dart';
import 'package:messenger_app/models/user.dart';
import 'package:mobx/mobx.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse extends _LoginResponse with _$LoginResponse {
  static LoginResponse fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  static Map<String, dynamic> toJson(LoginResponse loginResponse) =>
      _$LoginResponseToJson(loginResponse);
}

abstract class _LoginResponse with Store {
  String token;

  @observable
  User user;
}
