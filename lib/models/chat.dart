import 'package:json_annotation/json_annotation.dart';
import 'package:messenger_app/models/group.dart';
import 'package:messenger_app/models/message.dart';
import 'package:messenger_app/models/user.dart';
import 'package:mobx/mobx.dart';

part 'chat.g.dart';

@JsonSerializable()
class Chat extends _Chat with _$Chat {
  static Chat fromJson(Map<String, dynamic> json) => _$ChatFromJson(json);

  static Map<String, dynamic> toJson(Chat chat) => _$ChatToJson(chat);
}

abstract class _Chat with Store {
  @observable
  int chatId;

  @observable
  String chatName;

  @observable
  User user;

  @observable
  Group group;

  @observable
  ChatType chatType;

  @observable
  String createdAt;

  @observable
  Map<String,User> userMap;

  @observable
  Map<String,String> map;

  @observable
  String chatLogo;

  @observable
  Map<String, Message> messageMap;
}

enum ChatType { individual_chat, group_chat, broadcast_chat }
