// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Chat _$ChatFromJson(Map<String, dynamic> json) {
  return Chat()
    ..chatId = json['chatId'] as int
    ..chatName = json['chatName'] as String
    ..user = json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>)
    ..group = json['group'] == null
        ? null
        : Group.fromJson(json['group'] as Map<String, dynamic>)
    ..chatType = _$enumDecodeNullable(_$ChatTypeEnumMap, json['chatType'])
    ..createdAt = json['createdAt'] as String
    ..userMap = (json['userMap'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(
          k, e == null ? null : User.fromJson(e as Map<String, dynamic>)),
    )
    ..map = (json['map'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    )
    ..chatLogo = json['chatLogo'] as String
    ..messageMap = (json['messageMap'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(
          k, e == null ? null : Message.fromJson(e as Map<String, dynamic>)),
    );
}

Map<String, dynamic> _$ChatToJson(Chat instance) => <String, dynamic>{
      'chatId': instance.chatId,
      'chatName': instance.chatName,
      'user': instance.user,
      'group': instance.group,
      'chatType': _$ChatTypeEnumMap[instance.chatType],
      'createdAt': instance.createdAt,
      'userMap': instance.userMap,
      'map': instance.map,
      'chatLogo': instance.chatLogo,
      'messageMap': instance.messageMap,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$ChatTypeEnumMap = {
  ChatType.individual_chat: 'individual_chat',
  ChatType.group_chat: 'group_chat',
  ChatType.broadcast_chat: 'broadcast_chat',
};

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Chat on _Chat, Store {
  final _$chatIdAtom = Atom(name: '_Chat.chatId');

  @override
  int get chatId {
    _$chatIdAtom.context.enforceReadPolicy(_$chatIdAtom);
    _$chatIdAtom.reportObserved();
    return super.chatId;
  }

  @override
  set chatId(int value) {
    _$chatIdAtom.context.conditionallyRunInAction(() {
      super.chatId = value;
      _$chatIdAtom.reportChanged();
    }, _$chatIdAtom, name: '${_$chatIdAtom.name}_set');
  }

  final _$chatNameAtom = Atom(name: '_Chat.chatName');

  @override
  String get chatName {
    _$chatNameAtom.context.enforceReadPolicy(_$chatNameAtom);
    _$chatNameAtom.reportObserved();
    return super.chatName;
  }

  @override
  set chatName(String value) {
    _$chatNameAtom.context.conditionallyRunInAction(() {
      super.chatName = value;
      _$chatNameAtom.reportChanged();
    }, _$chatNameAtom, name: '${_$chatNameAtom.name}_set');
  }

  final _$userAtom = Atom(name: '_Chat.user');

  @override
  User get user {
    _$userAtom.context.enforceReadPolicy(_$userAtom);
    _$userAtom.reportObserved();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.context.conditionallyRunInAction(() {
      super.user = value;
      _$userAtom.reportChanged();
    }, _$userAtom, name: '${_$userAtom.name}_set');
  }

  final _$groupAtom = Atom(name: '_Chat.group');

  @override
  Group get group {
    _$groupAtom.context.enforceReadPolicy(_$groupAtom);
    _$groupAtom.reportObserved();
    return super.group;
  }

  @override
  set group(Group value) {
    _$groupAtom.context.conditionallyRunInAction(() {
      super.group = value;
      _$groupAtom.reportChanged();
    }, _$groupAtom, name: '${_$groupAtom.name}_set');
  }

  final _$chatTypeAtom = Atom(name: '_Chat.chatType');

  @override
  ChatType get chatType {
    _$chatTypeAtom.context.enforceReadPolicy(_$chatTypeAtom);
    _$chatTypeAtom.reportObserved();
    return super.chatType;
  }

  @override
  set chatType(ChatType value) {
    _$chatTypeAtom.context.conditionallyRunInAction(() {
      super.chatType = value;
      _$chatTypeAtom.reportChanged();
    }, _$chatTypeAtom, name: '${_$chatTypeAtom.name}_set');
  }

  final _$createdAtAtom = Atom(name: '_Chat.createdAt');

  @override
  String get createdAt {
    _$createdAtAtom.context.enforceReadPolicy(_$createdAtAtom);
    _$createdAtAtom.reportObserved();
    return super.createdAt;
  }

  @override
  set createdAt(String value) {
    _$createdAtAtom.context.conditionallyRunInAction(() {
      super.createdAt = value;
      _$createdAtAtom.reportChanged();
    }, _$createdAtAtom, name: '${_$createdAtAtom.name}_set');
  }

  final _$userMapAtom = Atom(name: '_Chat.userMap');

  @override
  Map<String, User> get userMap {
    _$userMapAtom.context.enforceReadPolicy(_$userMapAtom);
    _$userMapAtom.reportObserved();
    return super.userMap;
  }

  @override
  set userMap(Map<String, User> value) {
    _$userMapAtom.context.conditionallyRunInAction(() {
      super.userMap = value;
      _$userMapAtom.reportChanged();
    }, _$userMapAtom, name: '${_$userMapAtom.name}_set');
  }

  final _$mapAtom = Atom(name: '_Chat.map');

  @override
  Map<String, String> get map {
    _$mapAtom.context.enforceReadPolicy(_$mapAtom);
    _$mapAtom.reportObserved();
    return super.map;
  }

  @override
  set map(Map<String, String> value) {
    _$mapAtom.context.conditionallyRunInAction(() {
      super.map = value;
      _$mapAtom.reportChanged();
    }, _$mapAtom, name: '${_$mapAtom.name}_set');
  }

  final _$chatLogoAtom = Atom(name: '_Chat.chatLogo');

  @override
  String get chatLogo {
    _$chatLogoAtom.context.enforceReadPolicy(_$chatLogoAtom);
    _$chatLogoAtom.reportObserved();
    return super.chatLogo;
  }

  @override
  set chatLogo(String value) {
    _$chatLogoAtom.context.conditionallyRunInAction(() {
      super.chatLogo = value;
      _$chatLogoAtom.reportChanged();
    }, _$chatLogoAtom, name: '${_$chatLogoAtom.name}_set');
  }

  final _$messageMapAtom = Atom(name: '_Chat.messageMap');

  @override
  Map<String, Message> get messageMap {
    _$messageMapAtom.context.enforceReadPolicy(_$messageMapAtom);
    _$messageMapAtom.reportObserved();
    return super.messageMap;
  }

  @override
  set messageMap(Map<String, Message> value) {
    _$messageMapAtom.context.conditionallyRunInAction(() {
      super.messageMap = value;
      _$messageMapAtom.reportChanged();
    }, _$messageMapAtom, name: '${_$messageMapAtom.name}_set');
  }
}
