// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Group _$GroupFromJson(Map<String, dynamic> json) {
  return Group()
    ..groupId = json['groupId'] as int
    ..groupOwnerId = json['groupOwnerId'] as int
    ..groupDescription = json['groupDescription'] as String
    ..groupName = json['groupName'] as String;
}

Map<String, dynamic> _$GroupToJson(Group instance) => <String, dynamic>{
      'groupId': instance.groupId,
      'groupOwnerId': instance.groupOwnerId,
      'groupDescription': instance.groupDescription,
      'groupName': instance.groupName,
    };

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Group on _Group, Store {
  final _$groupIdAtom = Atom(name: '_Group.groupId');

  @override
  int get groupId {
    _$groupIdAtom.context.enforceReadPolicy(_$groupIdAtom);
    _$groupIdAtom.reportObserved();
    return super.groupId;
  }

  @override
  set groupId(int value) {
    _$groupIdAtom.context.conditionallyRunInAction(() {
      super.groupId = value;
      _$groupIdAtom.reportChanged();
    }, _$groupIdAtom, name: '${_$groupIdAtom.name}_set');
  }

  final _$groupOwnerIdAtom = Atom(name: '_Group.groupOwnerId');

  @override
  int get groupOwnerId {
    _$groupOwnerIdAtom.context.enforceReadPolicy(_$groupOwnerIdAtom);
    _$groupOwnerIdAtom.reportObserved();
    return super.groupOwnerId;
  }

  @override
  set groupOwnerId(int value) {
    _$groupOwnerIdAtom.context.conditionallyRunInAction(() {
      super.groupOwnerId = value;
      _$groupOwnerIdAtom.reportChanged();
    }, _$groupOwnerIdAtom, name: '${_$groupOwnerIdAtom.name}_set');
  }

  final _$groupDescriptionAtom = Atom(name: '_Group.groupDescription');

  @override
  String get groupDescription {
    _$groupDescriptionAtom.context.enforceReadPolicy(_$groupDescriptionAtom);
    _$groupDescriptionAtom.reportObserved();
    return super.groupDescription;
  }

  @override
  set groupDescription(String value) {
    _$groupDescriptionAtom.context.conditionallyRunInAction(() {
      super.groupDescription = value;
      _$groupDescriptionAtom.reportChanged();
    }, _$groupDescriptionAtom, name: '${_$groupDescriptionAtom.name}_set');
  }

  final _$groupNameAtom = Atom(name: '_Group.groupName');

  @override
  String get groupName {
    _$groupNameAtom.context.enforceReadPolicy(_$groupNameAtom);
    _$groupNameAtom.reportObserved();
    return super.groupName;
  }

  @override
  set groupName(String value) {
    _$groupNameAtom.context.conditionallyRunInAction(() {
      super.groupName = value;
      _$groupNameAtom.reportChanged();
    }, _$groupNameAtom, name: '${_$groupNameAtom.name}_set');
  }
}
