import 'package:flutter/material.dart';
import 'package:messenger_app/home_page.dart';
import 'package:messenger_app/stores/new_user_store.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';

class RegisterPage extends StatefulWidget {
  @override
  RegisterPageState createState() => RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  String email;
  String password;
  String retypePassword;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(body: Builder(builder: (BuildContext context) {
        return GestureDetector(
          child: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/bgimage.png'),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.95), BlendMode.dstATop),
              ),
            ),
            child: Center(
              child: Container(
                margin: EdgeInsets.all(20.0),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return null;
                              } else {
                                setState(() {
                                  email = value;
                                });

                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                hintText: 'Email', border: InputBorder.none),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return null;
                              } else {
                                setState(() {
                                  password = value;
                                });

                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                hintText: 'Password', border: InputBorder.none),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return null;
                              } else {
                                setState(() {
                                  retypePassword = value;
                                });

                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                hintText: 'Re-enter password',
                                border: InputBorder.none),
                          ),
                        ),
                        SizedBox(
                          height: 25.0,
                        ),
                        StoreObserver<NewUserStore>(
                          builder:
                              (NewUserStore userStore, BuildContext context) {
                            return RaisedButton(
                              onPressed: () {
                                _formKey.currentState.save();
                                if (_formKey.currentState.validate()) {
                                  userStore
                                      .isSigningUp(
                                    email: email,
                                    password: password,
                                    context: context,
                                  )
                                      .then((dynamic) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            HomePage(),
                                      ),
                                    );
                                  });
                                } else {
                                  _showSnackBar(
                                      context, 'Please provide valid entries');
                                }
                              },
                              child: Container(
                                margin: EdgeInsets.all(10.0),
                                height: 30,
                                child: Center(
                                  child: Text(
                                    'REGISTER',
                                    style: Styles.buttonText,
                                  ),
                                ),
                              ),
                              color: Colors.black,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      })),
    );
  }

  void _showSnackBar(BuildContext context, String content) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(content),
      ),
    );
  }
}
