import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:messenger_app/customs/custom_text_field.dart';
import 'package:messenger_app/stores/chat_store.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';

class AddGroupChat extends StatefulWidget {
  @override
  _AddGroupChatState createState() => _AddGroupChatState();
}

class _AddGroupChatState extends State<AddGroupChat> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  String groupName;
  String groupDescription;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Styles.primaryColor,
          title: Text(
            'Group description',
            style: Styles.appBarText,
          ),
        ),
        body: Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/form_bg.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: 10,
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                color: Colors.white,
              ),
              child: FormBuilder(
                key: _fbKey,
                autovalidate: false,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CustomTextField(
                        attribute: 'group_name',
                        validators: [FormBuilderValidators.required()],
                        hintText: 'Group Name',
                        entry: groupName,
                      ),
                      CustomTextField(
                        attribute: 'group_description',
                        validators: [FormBuilderValidators.required()],
                        hintText: 'Group Description',
                        entry: groupDescription,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      StoreObserver<ChatStore>(
                        builder: (ChatStore chatStore, BuildContext context) {
                          return RaisedButton(
                            color: Styles.primaryColor.withOpacity(0.5),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            onPressed: () {
                              _fbKey.currentState.save();
                              if (_fbKey.currentState.validate()) {
                                print('validated');
                                chatStore.addChatGroup(
                                    context,
                                    _fbKey.currentState.value['group_name'],
                                    _fbKey.currentState
                                        .value['group_description']);
                                Navigator.pop(context);
                              }
                            },
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              height: 30,
                              child: Center(
                                child: Text(
                                  'CREATE GROUP',
                                  style: Styles.buttonText,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
