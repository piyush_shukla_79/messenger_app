import 'package:flutter/material.dart';
import 'package:messenger_app/splash_page.dart';
import 'package:messenger_app/stores/chat_store.dart';
import 'package:messenger_app/stores/counter_store.dart';
import 'package:messenger_app/stores/message_store.dart';
import 'package:messenger_app/stores/new_user_store.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildCloneableWidget>[
        Provider<CounterStore>.value(value: CounterStore()),
        Provider<ChatStore>.value(value: ChatStore()),
        Provider<NewUserStore>.value(value: NewUserStore()),
        Provider<MessageStore>.value(value: MessageStore())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Scaffold(
            body: SplashPage(),
          ),
        ),
        title: 'Messenger App',
      ),
    );
  }
}
