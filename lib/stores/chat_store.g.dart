// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ChatStore on _ChatStore, Store {
  final _$chatMapAtom = Atom(name: '_ChatStore.chatMap');

  @override
  Map<String, Chat> get chatMap {
    _$chatMapAtom.context.enforceReadPolicy(_$chatMapAtom);
    _$chatMapAtom.reportObserved();
    return super.chatMap;
  }

  @override
  set chatMap(Map<String, Chat> value) {
    _$chatMapAtom.context.conditionallyRunInAction(() {
      super.chatMap = value;
      _$chatMapAtom.reportChanged();
    }, _$chatMapAtom, name: '${_$chatMapAtom.name}_set');
  }

  final _$randomAtom = Atom(name: '_ChatStore.random');

  @override
  Random get random {
    _$randomAtom.context.enforceReadPolicy(_$randomAtom);
    _$randomAtom.reportObserved();
    return super.random;
  }

  @override
  set random(Random value) {
    _$randomAtom.context.conditionallyRunInAction(() {
      super.random = value;
      _$randomAtom.reportChanged();
    }, _$randomAtom, name: '${_$randomAtom.name}_set');
  }

  final _$userMapAtom = Atom(name: '_ChatStore.userMap');

  @override
  Map<String, User> get userMap {
    _$userMapAtom.context.enforceReadPolicy(_$userMapAtom);
    _$userMapAtom.reportObserved();
    return super.userMap;
  }

  @override
  set userMap(Map<String, User> value) {
    _$userMapAtom.context.conditionallyRunInAction(() {
      super.userMap = value;
      _$userMapAtom.reportChanged();
    }, _$userMapAtom, name: '${_$userMapAtom.name}_set');
  }

  final _$addChatGroupAsyncAction = AsyncAction('addChatGroup');

  @override
  Future addChatGroup(BuildContext context, String name, String description) {
    return _$addChatGroupAsyncAction
        .run(() => super.addChatGroup(context, name, description));
  }

  final _$fetchChatsAsyncAction = AsyncAction('fetchChats');

  @override
  Future fetchChats() {
    return _$fetchChatsAsyncAction.run(() => super.fetchChats());
  }

  final _$addUserToChatAsyncAction = AsyncAction('addUserToChat');

  @override
  Future addUserToChat({Chat chat, User user}) {
    return _$addUserToChatAsyncAction
        .run(() => super.addUserToChat(chat: chat, user: user));
  }

  final _$fetchChatUsersAsyncAction = AsyncAction('fetchChatUsers');

  @override
  Future fetchChatUsers({Chat chat}) {
    return _$fetchChatUsersAsyncAction
        .run(() => super.fetchChatUsers(chat: chat));
  }

  final _$_ChatStoreActionController = ActionController(name: '_ChatStore');

  @override
  void clearStore() {
    final _$actionInfo = _$_ChatStoreActionController.startAction();
    try {
      return super.clearStore();
    } finally {
      _$_ChatStoreActionController.endAction(_$actionInfo);
    }
  }
}
