// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MessageStore on _MessageStore, Store {
  final _$messageMapAtom = Atom(name: '_MessageStore.messageMap');

  @override
  Map<String, Message> get messageMap {
    _$messageMapAtom.context.enforceReadPolicy(_$messageMapAtom);
    _$messageMapAtom.reportObserved();
    return super.messageMap;
  }

  @override
  set messageMap(Map<String, Message> value) {
    _$messageMapAtom.context.conditionallyRunInAction(() {
      super.messageMap = value;
      _$messageMapAtom.reportChanged();
    }, _$messageMapAtom, name: '${_$messageMapAtom.name}_set');
  }

  final _$randomAtom = Atom(name: '_MessageStore.random');

  @override
  Random get random {
    _$randomAtom.context.enforceReadPolicy(_$randomAtom);
    _$randomAtom.reportObserved();
    return super.random;
  }

  @override
  set random(Random value) {
    _$randomAtom.context.conditionallyRunInAction(() {
      super.random = value;
      _$randomAtom.reportChanged();
    }, _$randomAtom, name: '${_$randomAtom.name}_set');
  }

  final _$sendMessageAsyncAction = AsyncAction('sendMessage');

  @override
  Future sendMessage({Chat chat, String data}) {
    return _$sendMessageAsyncAction
        .run(() => super.sendMessage(chat: chat, data: data));
  }

  final _$fetchMessagesAsyncAction = AsyncAction('fetchMessages');

  @override
  Future fetchMessages({Chat chat}) {
    return _$fetchMessagesAsyncAction
        .run(() => super.fetchMessages(chat: chat));
  }
}
