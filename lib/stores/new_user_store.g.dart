// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_user_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NewUserStore on _NewUserStore, Store {
  final _$isLoadingAtom = Atom(name: '_NewUserStore.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$randomAtom = Atom(name: '_NewUserStore.random');

  @override
  Random get random {
    _$randomAtom.context.enforceReadPolicy(_$randomAtom);
    _$randomAtom.reportObserved();
    return super.random;
  }

  @override
  set random(Random value) {
    _$randomAtom.context.conditionallyRunInAction(() {
      super.random = value;
      _$randomAtom.reportChanged();
    }, _$randomAtom, name: '${_$randomAtom.name}_set');
  }

  final _$loggedInUserAtom = Atom(name: '_NewUserStore.loggedInUser');

  @override
  User get loggedInUser {
    _$loggedInUserAtom.context.enforceReadPolicy(_$loggedInUserAtom);
    _$loggedInUserAtom.reportObserved();
    return super.loggedInUser;
  }

  @override
  set loggedInUser(User value) {
    _$loggedInUserAtom.context.conditionallyRunInAction(() {
      super.loggedInUser = value;
      _$loggedInUserAtom.reportChanged();
    }, _$loggedInUserAtom, name: '${_$loggedInUserAtom.name}_set');
  }

  final _$loggedInUserDataAtom = Atom(name: '_NewUserStore.loggedInUserData');

  @override
  Map<String, dynamic> get loggedInUserData {
    _$loggedInUserDataAtom.context.enforceReadPolicy(_$loggedInUserDataAtom);
    _$loggedInUserDataAtom.reportObserved();
    return super.loggedInUserData;
  }

  @override
  set loggedInUserData(Map<String, dynamic> value) {
    _$loggedInUserDataAtom.context.conditionallyRunInAction(() {
      super.loggedInUserData = value;
      _$loggedInUserDataAtom.reportChanged();
    }, _$loggedInUserDataAtom, name: '${_$loggedInUserDataAtom.name}_set');
  }

  final _$userMapAtom = Atom(name: '_NewUserStore.userMap');

  @override
  Map<String, User> get userMap {
    _$userMapAtom.context.enforceReadPolicy(_$userMapAtom);
    _$userMapAtom.reportObserved();
    return super.userMap;
  }

  @override
  set userMap(Map<String, User> value) {
    _$userMapAtom.context.conditionallyRunInAction(() {
      super.userMap = value;
      _$userMapAtom.reportChanged();
    }, _$userMapAtom, name: '${_$userMapAtom.name}_set');
  }

  final _$isLoggingInAsyncAction = AsyncAction('isLoggingIn');

  @override
  Future isLoggingIn({String email, String password, BuildContext context}) {
    return _$isLoggingInAsyncAction.run(() =>
        super.isLoggingIn(email: email, password: password, context: context));
  }

  final _$isSigningUpAsyncAction = AsyncAction('isSigningUp');

  @override
  Future isSigningUp({String email, String password, BuildContext context}) {
    return _$isSigningUpAsyncAction.run(() =>
        super.isSigningUp(email: email, password: password, context: context));
  }

  final _$setLoggedInUserAsyncAction = AsyncAction('setLoggedInUser');

  @override
  Future setLoggedInUser() {
    return _$setLoggedInUserAsyncAction.run(() => super.setLoggedInUser());
  }

  final _$fetchUsersAsyncAction = AsyncAction('fetchUsers');

  @override
  Future fetchUsers() {
    return _$fetchUsersAsyncAction.run(() => super.fetchUsers());
  }

  final _$_NewUserStoreActionController =
      ActionController(name: '_NewUserStore');

  @override
  void clearStore() {
    final _$actionInfo = _$_NewUserStoreActionController.startAction();
    try {
      return super.clearStore();
    } finally {
      _$_NewUserStoreActionController.endAction(_$actionInfo);
    }
  }
}
