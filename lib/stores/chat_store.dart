import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:messenger_app/models/chat.dart';
import 'package:messenger_app/models/group.dart';
import 'package:messenger_app/models/user.dart';
import 'package:messenger_app/utils/globals.dart';
import 'package:mobx/mobx.dart';

part 'chat_store.g.dart';

class ChatStore = _ChatStore with _$ChatStore;

abstract class _ChatStore with Store {
  @observable
  Map<String, Chat> chatMap;

  @observable
  Random random = Random();

  @observable
  Map<String, User> userMap;

  @action
  dynamic addChatGroup(
      BuildContext context, String name, String description) async {
    User user =
        await firebaseUserService.fetchCurrentUserDetails().then((value) {
      return value;
    });
    Chat chat = Chat();
    chat.group = Group();
    chat.map = Map<String, String>();
    chat.chatId = random.nextInt(1000);
    chat.group.groupOwnerId =
        await firebaseUserService.fetchCurrentUserDetails().then((value) {
      return value.id;
    });
    chat.chatLogo = 'assets/chat_image.jpg';
    chat.group.groupName = name;
    chat.group.groupDescription = description;
    chat.map.addAll(<String, String>{user.id.toString(): user.fullName});
    chat.createdAt = DateFormat.yMd().add_jm().format(DateTime.now());
    await addGroupService.addChat(chat: chat).then((dynamic) async {
      await addGroupService
          .addUserToChat(chat: chat, user: user)
          .then((value) async {
        Map<String, User> userMap =
            await addGroupService.fetchChatMembers(chat: chat).then((value) {
          return value;
        });
        Map<String, String> map = Map<String, String>();
        userMap.forEach((k, v) {
          map.addAll(<String, String>{k: v.fullName});
        });
        chat.map = map;
        await Firestore.instance
            .collection('chat-groups')
            .document(chat.chatId.toString())
            .updateData({
          'chat_usermap': chat.map,
        });
      });

      print('chat added');
    });
  }

  @action
  dynamic fetchChats() async {
    chatMap = await addGroupService.getAllChats().then((value) {
      return value;
    });
  }

  @action
  dynamic addUserToChat({Chat chat, User user}) async {
    await addGroupService
        .addUserToChat(chat: chat, user: user)
        .then((dynamic) async {
      Map<String, User> userMap =
          await addGroupService.fetchChatMembers(chat: chat).then((value) {
        return value;
      });
      Map<String, String> map = Map<String, String>();
      userMap.forEach((k, v) {
        map.addAll(<String, String>{k: v.fullName});
      });
      chat.map = map;
      await Firestore.instance
          .collection('chat-groups')
          .document(chat.chatId.toString())
          .updateData({
        'chat_usermap': chat.map,
      });
      print('user added to chat');
    });
  }

  @action
  dynamic fetchChatUsers({Chat chat}) async {
    userMap = await addGroupService.fetchChatMembers(chat: chat).then((value) {
      return value;
    });
  }

  @action
  void clearStore(){
    chatMap = null;
    userMap = null;
  }
}
