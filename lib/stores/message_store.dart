import 'dart:math';

import 'package:messenger_app/models/chat.dart';
import 'package:messenger_app/models/message.dart';
import 'package:messenger_app/models/user.dart';
import 'package:messenger_app/utils/globals.dart';
import 'package:mobx/mobx.dart';

part 'message_store.g.dart';

class MessageStore = _MessageStore with _$MessageStore;

abstract class _MessageStore with Store {
  @observable
  Map<String, Message> messageMap;

  @observable
  Random random = Random();

  @action
  dynamic sendMessage({Chat chat, String data}) async {
    User user =
        await firebaseUserService.fetchCurrentUserDetails().then((value) {
      return value;
    });
    Message message = Message();
    message.messageId = random.nextInt(100000);
    message.data = data;
    message.senderId = user.id;
    message.timeStamp = DateTime.now().toString();
    message.senderName = user.fullName;
    await messageService
        .onSendMessage(chat: chat, message: message)
        .then((value) {
      print('message sent');
    });
  }

  @action
  dynamic fetchMessages({Chat chat}) async {
    messageMap =
        await messageService.fetchChatMessages(chat: chat).then((value) {
      return value;
    });
  }
}
