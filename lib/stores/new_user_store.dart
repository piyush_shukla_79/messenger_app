import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messenger_app/login_page.dart';
import 'package:messenger_app/models/user.dart';
import 'package:messenger_app/utils/globals.dart';
import 'package:mobx/mobx.dart';

import '../models/user.dart';

part 'new_user_store.g.dart';

class NewUserStore = _NewUserStore with _$NewUserStore;

abstract class _NewUserStore with Store {
  @observable
  bool isLoading = false;

  @observable
  Random random = Random();

  @observable
  User loggedInUser;

  @observable
  Map<String, dynamic> loggedInUserData;

  @observable
  Map<String, User> userMap;

  @action
  dynamic isLoggingIn(
      {String email, String password, BuildContext context}) async {
    isLoading = true;
    print('is logging in');
    await firebaseUserService
        .login(email: email, password: password)
        .then((FirebaseUser firebaseUser) async {
      print('on login user data printing');
      loggedInUser =
          await firebaseUserService.fetchCurrentUserDetails().then((value) {
        return value;
      });
      isLoading = false;
    }).catchError((e) {
      print('error logging in $e');
    });
    isLoading = false;
  }

  @action
  dynamic isSigningUp(
      {String email, String password, BuildContext context}) async {
    isLoading = true;
    print('is signing up');
    User user = User();
    user.id = random.nextInt(1000);
    user.firstName = 'Piyush';
    user.lastName = 'Shukla ${user.id}';
    user.age = 19;
    user.gender = 'Male';
    await firebaseUserService
        .signUp(email: email, password: password, context: context, user: user)
        .then((FirebaseUser firebaseUser) async {
      print('on sign up data printing');
      print(firebaseUser.uid);
      loggedInUser =
          await firebaseUserService.fetchCurrentUserDetails().then((value) {
        return value;
      });
      isLoading = false;
    }).catchError((e) {
      print('error signing up $e');
    });
    isLoading = false;
  }

  @action
  dynamic setLoggedInUser() async {
    isLoading = true;
    loggedInUser =
        await firebaseUserService.fetchCurrentUserDetails().then((value) {
      return value;
    });
    isLoading = false;
  }

  @action
  dynamic fetchUsers() async {
    userMap = await firebaseUserService.fetchAllUsers().then((users) {
      return users;
    });
  }

  @action
  void clearStore() {
    loggedInUser = null;
    userMap = null;
  }
}
