import 'package:flutter/material.dart';
import 'package:messenger_app/customs/custom_chat_tile.dart';
import 'package:messenger_app/models/chat.dart';
import 'package:messenger_app/stores/chat_store.dart';
import 'package:messenger_app/stores/new_user_store.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';
import 'package:provider/provider.dart';

class AddChatUsersPage extends StatefulWidget {
  final Chat chat;

  AddChatUsersPage({this.chat});

  @override
  _AddChatUsersPageState createState() => _AddChatUsersPageState();
}

class _AddChatUsersPageState extends State<AddChatUsersPage> {
  List<int> selectedUserList = List<int>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return StoreObserver<NewUserStore>(
      builder: (NewUserStore userStore, BuildContext context) {
        userStore.fetchUsers();
        Provider.of<ChatStore>(context).fetchChatUsers(chat: widget.chat);
        return Scaffold(
          appBar: AppBar(
            title: Text(
              selectedUserList.isEmpty
                  ? 'Add users to chat'
                  : selectedUserList.length.toString() + ' Selected',
              style: Styles.appBarText,
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.check_box,
                  color: selectedUserList.isEmpty ? Colors.grey : Colors.green,
                ),
                onPressed: () {
                  selectedUserList.forEach((value) {
                    Provider.of<ChatStore>(context).addUserToChat(
                      user: userStore.userMap[value.toString()],
                      chat: widget.chat,
                    );
                  });
                  Navigator.pop(context);
                },
              ),
            ],
          ),
          body: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            child: userStore.userMap == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    itemCount: userStore.userMap.values.toList().length,
                    itemBuilder: (BuildContext context, int index) {
                      return CustomChatTile(
                        checkbox: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Styles.primaryColor,
                              width: 1.5,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                          child: selectedUserList.contains(
                                  userStore.userMap.values.toList()[index].id)
                              ? Icon(
                                  Icons.check,
                                  size: 15,
                                  color: Colors.green,
                                )
                              : SizedBox(),
                        ),
                        isEditing: true,
                        avatarUrl: 'assets/chat_image.jpg',
                        title:
                            userStore.userMap.values.toList()[index].fullName,
                        subtitle: userStore.userMap.values
                            .toList()[index]
                            .id
                            .toString(),
                        time: "Age: " +
                            userStore.userMap.values
                                .toList()[index]
                                .age
                                .toString(),
                        onTap: () {
                          if (selectedUserList.contains(
                              userStore.userMap.values.toList()[index].id)) {
                            selectedUserList.remove(
                                userStore.userMap.values.toList()[index].id);
                          } else {
                            selectedUserList.add(
                                userStore.userMap.values.toList()[index].id);
                          }
                        },
                      );
                    },
                  ),
          ),
        );
      },
    );
  }
}
