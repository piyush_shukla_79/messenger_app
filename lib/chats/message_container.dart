import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/styles.dart';

class MessageContainer extends StatelessWidget {
  final String text;
  final String name;
  final String time;
  final bool isLoggedInUser;
  final bool shouldDisplayName;
  final bool areSameDayMessages;

  MessageContainer({
    this.text,
    this.isLoggedInUser,
    this.name,
    this.time,
    this.shouldDisplayName,
    this.areSameDayMessages,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: <Widget>[
        !areSameDayMessages
            ? Container(
                width: SizeConfig.screenWidth,
                height: SizeConfig.screenHeight * 0.05,
                color: Styles.primaryColor.withOpacity(0.5),
                child: Center(
                  child: Text(
                    DateFormat.yMMMMd().format(
                      DateTime.parse(time),
                    ),
                    style: Styles.buttonText,
                  ),
                ),
              )
            : SizedBox(),
        Container(
          alignment:
              isLoggedInUser ? Alignment.centerRight : Alignment.centerLeft,
          child: Stack(
            children: <Widget>[
              Card(
                color: isLoggedInUser
                    ? Color.fromRGBO(194, 250, 167, 1)
                    : Colors.white,
                margin: EdgeInsets.symmetric(
                  horizontal: 10.0,
                  vertical: 5.0,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    shouldDisplayName
                        ? Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: 10.0,
                              vertical: 5.0,
                            ),
                            child: Text(
                              name,
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: Styles.primaryColor,
                              ),
                            ),
                          )
                        : SizedBox(),
                    Container(
                      constraints: BoxConstraints(
                        minWidth: SizeConfig.screenWidth * 0.35,
                        maxWidth: SizeConfig.screenWidth * 0.70,
                      ),
                      margin: EdgeInsets.symmetric(
                        horizontal: 10.0,
                        vertical: 5.0,
                      ),
                      child: Text(
                        text,
                        style: TextStyle(
                          fontSize: 14,
                        ),
                        maxLines: null,
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 10.0,
                bottom: 10.0,
                child: Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 5.0,
                  ),
                  child: Text(
                    DateFormat.jm().format(
                      DateTime.parse(time),
                    ),
                    style: TextStyle(
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.end,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
