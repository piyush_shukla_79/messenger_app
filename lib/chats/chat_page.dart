import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:messenger_app/chats/chat_detail_page.dart';
import 'package:messenger_app/chats/message_container.dart';
import 'package:messenger_app/models/chat.dart';
import 'package:messenger_app/stores/chat_store.dart';
import 'package:messenger_app/stores/message_store.dart';
import 'package:messenger_app/stores/new_user_store.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';
import 'package:provider/provider.dart';

class ChatPage extends StatefulWidget {
  final int id;
  final Chat chat;

  ChatPage({
    this.id,
    this.chat,
  });

  @override
  ChatPageState createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> with TickerProviderStateMixin {
  TextEditingController _textController = new TextEditingController();
  String text;

  List<MessageContainer> messages = <MessageContainer>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    ChatDetailPage(
                      id: widget.id,
                    ),
              ),
            );
          },
          child: AppBar(
            backgroundColor: Styles.primaryColor,
            title: Text(
              'Chat id: ' + widget.id.toString(),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
      ),
      body: StoreObserver<MessageStore>(
        builder: (MessageStore messageStore, BuildContext context) {
          messageStore.fetchMessages(chat: widget.chat);
          Provider.of<NewUserStore>(context).setLoggedInUser();
          Provider.of<ChatStore>(context).fetchChatUsers(chat: widget.chat);
          Provider.of<NewUserStore>(context).fetchUsers();
          return messageStore.messageMap == null ||
              Provider
                  .of<NewUserStore>(context)
                  .loggedInUser == null
              ? Center(
            child: CircularProgressIndicator(),
          )
              : Column(
            children: <Widget>[
              Expanded(
                child: ListView.builder(
                    itemCount:
                    messageStore.messageMap.keys
                        .toList()
                        .length,
                    reverse: true,
                    itemBuilder: (BuildContext context, int index) {
                      return MessageContainer(
                          text: messageStore.messageMap.values
                              .toList()[index]
                              .data,
                          name: messageStore.messageMap.values
                              .toList()[index]
                              .senderName,
                          time: messageStore.messageMap.values
                              .toList()[index]
                              .timeStamp,
                          isLoggedInUser: messageStore.messageMap.values
                              .toList()[index]
                              .senderId ==
                              Provider
                                  .of<NewUserStore>(context)
                                  .loggedInUser
                                  .id,
                          shouldDisplayName: messageStore.messageMap.values
                              .toList()
                              .length <
                              2
                              ? true
                              : index + 1 ==
                              messageStore.messageMap.values
                                  .toList()
                                  .length
                              ? true
                              : messageStore.messageMap.values
                              .toList()[index]
                              .senderId !=
                              messageStore.messageMap.values
                                  .toList()[index + 1]
                                  .senderId,
                          areSameDayMessages: messageStore.messageMap.values
                              .toList()
                              .length <
                              2
                              ? false
                              : index + 1 ==
                              messageStore.messageMap.values
                                  .toList()
                                  .length
                              ? false
                              : DateFormat.yMMMMd().format(DateTime.parse(
                              messageStore.messageMap.values
                                  .toList()[index]
                                  .timeStamp)) == DateFormat.yMMMMd().format(
                              DateTime.parse(messageStore.messageMap.values
                                  .toList()[index + 1]
                                  .timeStamp))
                      );
                    }),
              ),
              Container(
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    Flexible(
                      child: Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(
                          horizontal: 10.0,
                        ),
                        margin: EdgeInsets.only(
                          left: 10.0,
                        ),
                        height: 45.0,
                        decoration: BoxDecoration(
                            border: Border.all(width: 1),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            color: Colors.white),
                        child: Center(
                          child: TextField(
                            decoration: InputDecoration.collapsed(
                              hintText: 'Write here',
                              border: InputBorder.none,
                            ),
                            controller: _textController,
                            onChanged: (value) {
                              text = value;
                            },
                          ),
                        ),
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () {
                        _submitMessage(_textController.text, context);
                      },
                    ),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }

  void _submitMessage(String text, BuildContext context) {
    MessageStore messageStore = Provider.of<MessageStore>(context);
    ChatStore chatStore = Provider.of<ChatStore>(context);
    chatStore.fetchChats();
    messageStore.sendMessage(
      chat: chatStore.chatMap[widget.id.toString()],
      data: text,
    );
    _textController.clear();
    setState(() {
      messages.insert(
        0,
        MessageContainer(
          text: text,
        ),
      );
    });
  }
}
