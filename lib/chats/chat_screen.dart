import 'package:flutter/material.dart';
import 'package:messenger_app/chats/chat_page.dart';
import 'package:messenger_app/customs/custom_chat_tile.dart';
import 'package:messenger_app/stores/chat_store.dart';
import 'package:messenger_app/user_manager/add_group_chat.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return StoreObserver<ChatStore>(
      builder: (ChatStore chatStore, BuildContext context) {
        chatStore.fetchChats();
        return Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            child: chatStore.chatMap == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    itemCount: chatStore.chatMap.keys.length,
                    itemBuilder: (BuildContext context, int index) {
                      return CustomChatTile(
                        isEditing: false,
                        title: chatStore.chatMap.values
                            .toList()[index]
                            .group
                            .groupName,
                        subtitle: chatStore.chatMap.values
                            .toList()[index]
                            .group
                            .groupDescription,
                        avatarUrl: 'assets/chat_image.jpg',
                        time:
                            chatStore.chatMap.values.toList()[index].createdAt,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => ChatPage(
                                id: chatStore.chatMap.values
                                    .toList()[index]
                                    .chatId,
                                chat: chatStore.chatMap.values.toList()[index],
                              ),
                            ),
                          );
                        },
                        onLongPress: () {},
                      );
                    },
                  ),
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Styles.primaryColor,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => AddGroupChat(),
                ),
              );
            },
            child: Icon(Icons.add),
          ),
        );
      },
    );
  }
}
