import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messenger_app/chats/add_chat_users_page.dart';
import 'package:messenger_app/stores/chat_store.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';

class ChatDetailPage extends StatefulWidget {
  final int id;

  ChatDetailPage({this.id});

  @override
  _ChatDetailPageState createState() => _ChatDetailPageState();
}

class _ChatDetailPageState extends State<ChatDetailPage> {
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return StoreObserver<ChatStore>(
      builder: (ChatStore chatStore, BuildContext context) {
        chatStore.fetchChats();
        chatStore.fetchChatUsers(
          chat: chatStore.chatMap[widget.id.toString()],
        );
        return Scaffold(
          appBar: AppBar(
            title: Text(
              'Chat Id: ' + widget.id.toString(),
              style: Styles.appBarText,
            ),
            backgroundColor: Styles.primaryColor,
          ),
          body: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: <Widget>[
                  Container(
                    height: SizeConfig.screenHeight * 0.4,
                    child: Image.asset('assets/chat_image.jpg'),
                  ),
                  Divider(),
                  headerContainer(
                    header: 'Group Details',
                    sideIcon: IconButton(
                      icon: Icon(
                        Icons.edit,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                  ),
                  Divider(),
                  SizedBox(height: 10.0),
                  groupDetailsContainer(chatStore: chatStore),
                  SizedBox(height: 10.0),
                  Divider(),
                  headerContainer(
                    header: 'Members',
                    sideIcon: IconButton(
                      icon: Icon(
                        Icons.person_add,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => AddChatUsersPage(
                              chat: chatStore.chatMap[widget.id.toString()],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Divider(),
                  SizedBox(height: 10.0),
                  memberContainer(chatStore: chatStore)
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget groupDetailsContainer({ChatStore chatStore}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        detailContainer(
          header: 'Name',
          response: chatStore.chatMap[widget.id.toString()].group.groupName,
        ),
        Divider(),
        detailContainer(
          header: 'Description',
          response:
              chatStore.chatMap[widget.id.toString()].group.groupDescription,
        ),
        Divider(),
        detailContainer(
          header: 'Total participants',
          response: '10',
        ),
        Divider(),
        detailContainer(
          header: 'Created at',
          response: chatStore.chatMap[widget.id.toString()].createdAt,
        ),
      ],
    );
  }

  Widget detailContainer({String header, String response}) {
    return Container(
      height: SizeConfig.screenHeight * 0.1,
      margin: EdgeInsets.symmetric(
        horizontal: 20.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            header.toUpperCase(),
            style: Styles.headerText,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            response,
            style: Styles.responseText,
          ),
        ],
      ),
    );
  }

  Widget memberContainer({ChatStore chatStore}) {
    return chatStore.userMap == null
        ? Center(
            child: CircularProgressIndicator(),
          )
        : ListView.builder(
            controller: scrollController,
            shrinkWrap: true,
            itemCount: chatStore.userMap.keys.toList().length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: <Widget>[
                  memberRow(
                    name: chatStore.userMap.values.toList()[index].fullName,
                  ),
                  Divider(),
                ],
              );
            },
          );
  }

  Widget memberRow({String name}) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 20.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: SizeConfig.screenWidth * 0.12,
            height: SizeConfig.screenWidth * 0.12,
            margin: EdgeInsets.only(right: 20.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: Styles.primaryColor, width: 2.5),
              image: DecorationImage(
                  image: AssetImage('assets/chat_image.jpg'),
                  fit: BoxFit.cover),
            ),
          ),
          Text(
            name,
            style: Styles.hintText,
          ),
        ],
      ),
    );
  }

  Widget headerContainer({String header, Widget sideIcon}) {
    return Container(
      width: SizeConfig.screenWidth,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      height: SizeConfig.screenHeight * 0.08,
      alignment: Alignment.centerLeft,
      color: Styles.primaryColor.withOpacity(0.5),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              header.toUpperCase(),
              style: Styles.buttonText,
            ),
          ),
          sideIcon
        ],
      ),
    );
  }
}
