import 'package:flutter/material.dart';
import 'package:messenger_app/login_page.dart';
import 'package:messenger_app/stores/chat_store.dart';
import 'package:messenger_app/stores/counter_store.dart';
import 'package:messenger_app/stores/new_user_store.dart';
import 'package:messenger_app/utils/globals.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';
import 'package:provider/provider.dart';

class CounterPage extends StatefulWidget {
  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return StoreObserver<CounterStore>(
      builder: (CounterStore counterStore, BuildContext context) {
        return Scaffold(
          body: Container(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  counterStore.counter.toString(),
                ),
                Container(
                  margin: EdgeInsets.all(20.0),
                  child: RaisedButton(
                    color: Styles.primaryColor.withOpacity(0.5),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    onPressed: () {
                      clearStore(context);
                    },
                    child: Container(
                      margin: EdgeInsets.all(10.0),
                      height: 30,
                      child: Center(
                        child: Text(
                          'LOGOUT',
                          style: Styles.buttonText,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              counterStore.incrementAction();
              print(counterStore.counter);
            },
            child: Icon(Icons.add),
          ),
        );
      },
    );
  }

  void clearStore(BuildContext context) {
    firebaseUserService.logOut().then((dynamic) {
      Provider.of<NewUserStore>(context).clearStore();
      Provider.of<ChatStore>(context).clearStore();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => LoginPage(),
        ),
      );
    });
  }
}
