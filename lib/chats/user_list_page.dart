import 'package:flutter/material.dart';
import 'package:messenger_app/customs/custom_chat_tile.dart';
import 'package:messenger_app/stores/new_user_store.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';

class UserListPage extends StatefulWidget {
  final bool isEditing;

  UserListPage({
    this.isEditing,
  });

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        child: StoreObserver<NewUserStore>(
          builder: (NewUserStore userStore, BuildContext context) {
            userStore.fetchUsers();
            return userStore.userMap == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    itemCount: userStore.userMap.values.toList().length,
                    itemBuilder: (BuildContext context, int index) {
                      return CustomChatTile(
                        isEditing: widget.isEditing,
                        avatarUrl: 'assets/chat_image.jpg',
                        title:
                            userStore.userMap.values.toList()[index].fullName,
                        subtitle: userStore.userMap.values
                            .toList()[index]
                            .id
                            .toString(),
                        time: "Age: " +
                            userStore.userMap.values
                                .toList()[index]
                                .age
                                .toString(),
                        onTap: () {},
                      );
                    },
                  );
          },
        ),
      ),
    );
  }
}
