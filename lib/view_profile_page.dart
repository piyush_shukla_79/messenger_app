import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:messenger_app/stores/new_user_store.dart';
import 'package:messenger_app/utils/sizeconfig.dart';
import 'package:messenger_app/utils/store_observer.dart';
import 'package:messenger_app/utils/styles.dart';

class ViewProfilePage extends StatefulWidget {
  @override
  _ViewProfilePageState createState() => _ViewProfilePageState();
}

class _ViewProfilePageState extends State<ViewProfilePage> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Styles.primaryColor,
        title: Text(
          'Profile Page',
          style: Styles.appBarText,
        ),
        actions: <Widget>[IconButton(icon: Icon(Icons.edit), onPressed: () {})],
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        margin: EdgeInsets.all(10.0),
        child: StoreObserver<NewUserStore>(
            builder: (NewUserStore userStore, BuildContext context) {
          userStore.setLoggedInUser();
          return userStore.loggedInUser == null
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: CircleAvatar(
                          radius: 50,
                          child: Center(
                            child: Icon(
                              Icons.person,
                              size: 75,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Text(
                          userStore.loggedInUser.fullName,
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      Container(
                        width: SizeConfig.screenWidth * 0.6,
                        margin: EdgeInsets.all(10.0),
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Styles.primaryColor,
                            width: 2.5,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                        ),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              userDataRow(
                                  'Age', userStore.loggedInUser.age.toString()),
                              userDataRow(
                                  'Gender', userStore.loggedInUser.gender),
                              userDataRow(
                                  'Id', userStore.loggedInUser.id.toString())
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
        }),
      ),
    );
  }

  Widget userDataRow(String header, String response) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: SizeConfig.screenWidth * 0.3,
            child: Text(
              header + ': ',
              style: Styles.headerText,
            ),
          ),
          Container(
            width: SizeConfig.screenWidth * 0.15,
            child: Text(
              response,
              style: Styles.responseText,
            ),
          )
        ],
      ),
    );
  }
}
