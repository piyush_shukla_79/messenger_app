abstract class StringValue {
  static const String MY_CHAT_UP = "My Chat App";
  static const String LOG_IN_BUTTON = "Login";
  static const String SIGN_UP_BUTTON = "Sign Up";
  static const String ADD_USER = "Add User";
  static const String CAMERA = "Camera";
  static const String CHATS = "Chats";
  static const String STATUS = "Status";
  static const String CONTACTS = "Contacts";
}
