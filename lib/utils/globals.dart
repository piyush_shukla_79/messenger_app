import 'package:messenger_app/services/add_group_service.dart';
import 'package:messenger_app/services/firebase_user_service.dart';
import 'package:messenger_app/services/message_service.dart';
import 'package:messenger_app/services/preferences_service.dart';

final FirebaseUserService firebaseUserService =
    FirebaseUserService.getInstance();

final PreferencesService preferencesService = PreferencesService.getInstance();

final AddGroupService addGroupService = AddGroupService.getInstance();

final MessageService messageService = MessageService.getInstance();
